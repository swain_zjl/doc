[TOC]

# centos7.6安装mysql8.0教程

**系统环境 centos7.6**

```
# 查看当前centos版本
cat /etc/redhat-release
```

![image-20220310092853906](img/centos7.6安装mysql8.0教程/image-20220310092853906.png)



## 1、清洗环境

### 移除软件包

移除后可查询是否移除成功

```
# 查询mysql软件包
rpm -qa | grep mysql

# 移除软件包
yum remove mysql80-community-release-el7-5.noarch
```

![image-20220218090612473](img/centos7.6安装mysql8.0教程/image-20220218090612473.png)



### 移除安装配置

删除后可查询是否删除成功

```
# 查询配置
find / -name mysql

# 删除配置
rm -rf [文件夹路径]
```

![image-20220218090842123](img/centos7.6安装mysql8.0教程/image-20220218090842123.png)



### 移除mariadb

```
# 查询软件包
rpm -qa | grep mariadb

# 移除
rpm -e [软件包名称]

# 强制移除
rpm -e --nodeps [软件包名称]
```



## 2、安装

### 下载文件

**可以到官网https://dev.mysql.com/downloads/repo/yum/下载离线rpm，或者使用wget下载到对应目录**

1、打开官网download地址，点击对应版本下载

![image-20220218092057347](img/centos7.6安装mysql8.0教程/image-20220218092057347.png)

2、点击直接下载，或者右键复制下载地址，使用wget下载

![image-20220218092222166](img/centos7.6安装mysql8.0教程/image-20220218092222166.png)

**wget 下载命令**

```
# 下载
wget https://dev.mysql.com/get/mysql80-community-release-el7-5.noarch.rpm
```

![image-20220218092444915](img/centos7.6安装mysql8.0教程/image-20220218092444915.png)



### yum

**生成仓库文件**

```
# 执行生成
 rpm  -ivh  mysql80-community-release-el7-3.noarch.rpm
 
# 查看生成文件
ls /etc/yum.repos.d/
```

![image-20220218093000498](img/centos7.6安装mysql8.0教程/image-20220218093000498.png)

![image-20220218093114841](img/centos7.6安装mysql8.0教程/image-20220218093114841.png)

**更新yum缓存**

```
# 清理缓存
yum clean all 

# 拉取缓存
yum makecache
```

![image-20220218093337388](img/centos7.6安装mysql8.0教程/image-20220218093337388.png)

### 执行安装

```
# 安装mysql
yum -y install mysql-community-server

# 查看版本
mysql -V
```

![image-20220218093813415](img/centos7.6安装mysql8.0教程/image-20220218093813415.png)



## 3、配置

### 开机启动设置

```
# 加入开机启动
systemctl enable mysqld.service
```



### 服务状态控制

```
# 查看服务状态
systemctl status mysqld.service

# 启动服务
systemctl start  mysqld.service

# 停止服务
systemctl stop mysqld.service

# 重启服务
service mysqld restart
```



### 端口配置

**修改配置文件**

```
# 修改my.cnf
vi /etc/my.cnf

# 端口配置
port=4000

# 忽略大小写
lower_case_table_names=1

# 保存并重启
service mysqld restart
```

![image-20220218104352387](img/centos7.6安装mysql8.0教程/image-20220218104352387.png)



**需要远程连接，则需要防火墙+安全组放行**

```
# 加入放行端口
firewall-cmd --zone=public --add-port=[port]/tcp --permanent

# 重载配置
firewall-cmd --reload
```

![image-20220218103130129](img/centos7.6安装mysql8.0教程/image-20220218103130129.png)

### 登录用户配置

**root初始化登录**

```
# 查询root初始密码
grep "password" /var/log/mysqld.log

# 登录
mysql -uroot -p

# 查看密码安全等级
SHOW VARIABLES LIKE 'validate_password%'; 

# 设置密码安全等级
set global validate_password.policy=0;

# 修改密码
alter user 'root'@'localhost' identified  by '你的密码';
```

![image-20220218100241641](img/centos7.6安装mysql8.0教程/image-20220218100241641.png)

![image-20220218104753637](img/centos7.6安装mysql8.0教程/image-20220218104753637.png)

![image-20220218105026467](img/centos7.6安装mysql8.0教程/image-20220218105026467.png)

**修改密码**

```
# 修改密码
alter user '你的用户名'@'%' identified with mysql_native_password by '你的密码';

# 刷新缓存
flush  privileges;
```



**远程连接**

```
# 设置root远程连接
update user set host='%' where user = 'root'; 

# 刷新缓存
flush  privileges;
```



## 4、测试

### navicat连接测试

![image-20220218104424759](img/centos7.6安装mysql8.0教程/image-20220218104424759.png)
